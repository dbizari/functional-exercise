export const wordCount = (sequence: string): Record<string, number> => {
  sequence = sequence.trim()
  if (sequence == "") return {}

  let words = sequence.split(' ')

  return words.map(function(s: string) : Record<string, number> {
    s = s.toLocaleLowerCase()
    var aux : Record<string, number> = {}
    aux[s] = 1
    return aux
  }).reduce(function(prev: Record<string, number>, curr: Record<string, number>) : Record<string, number> {
    if (Object.keys(curr)[0] in prev) {
      prev[Object.keys(curr)[0]] = prev[Object.keys(curr)[0]] + 1
    } else {
      prev[Object.keys(curr)[0]] = 1
    }

    return prev
  })
};
